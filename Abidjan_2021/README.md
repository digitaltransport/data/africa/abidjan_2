*(en Français ci-dessous)*

Project launched in 2019 for [the Ivory Coast’s Ministry of Transport](http://www.transports.gouv.ci/), financed by [AFD](https://www.afd.fr/), realized by [Jungle Bus](https://junglebus.io/), [Systra](https://www.systra.com/fr/) and the local [OpenStreetMap Ivory Coast association (OSM-CI)](http://openstreetmap.ci/).
The objective is to map the formal and informal transport of Abidjan, the country’s growing capital, in OpenStreetMap and generate a [GTFS](https://gitlab.com/digitaltransport/data/africa/abidjan_2/-/blob/main/Abidjan_2021/Donn%C3%A9es/abidjan.zip?ref_type=heads) from it.

The whole project is written in French.

-----

Projet réalisé entre 2019 et 2020 pour le compte du [Ministère des Transport de Côte d'Ivoire](http://www.transports.gouv.ci/), financé par l'[AFD](https://www.afd.fr/), réalisé par [Jungle Bus](https://junglebus.io/), [Systra](https://www.systra.com/fr/) et l'[association locale OpenStreetMap Côte d'Ivoire (OSM-CI)](http://openstreetmap.ci/).

L'objectif est de cartographier l'ensemble des réseaux de transport officiels et informels du district d'Abidjan dans OpenStreetMap puis de produire un fichier GTFS. En parallèle, les acteurs locaux sont formés pour s'approprier les technologies et méthodologies OpenStreetMap.

En 2021, une mise à jour a été réalisée : une centaine de nouvelles lignes de transport informel (gbaka et woro woro) ont été identifiées et cartographiées dans OpenStreetMap afin de produire un nouveau GTFS.

## Livrables

Les données produites dans le cadre du projet (dont un fichier GTFS) sont disponibles dans le dossier [Données](Données/README.md). 

Le plan schématique produit est disponible dans [Plan](Plan/README.md). 

Ces données sont également visibles sur les fonds transport d'OpenStreetMap : https://www.openstreetmap.org/#map=12/5.3289/-4.0229&layers=T

## Documentation
La documentation du projet est disponible sur le site suivant : https://gitlab.com/digitaltransport/sites/abidjantransport

Toutes les informations pour participer au projet (la méthodologie de collecte des données sur le terrain, le processus de retranscription dans OpenStreetMap, le contrôle qualité etc..). sont disponibles sur la page [OSM Wiki](https://wiki.openstreetmap.org/wiki/FR:WikiProject_C%C3%B4te_d'Ivoire/Transport_Abidjan).

Les supports de formations sont disponibles dans le dossier [Formations](Formations/README.md).
