This repository contains all documentation and datasets produced throughout the Smart mapping project initiated by [Ewarren Financial Services SARL Cote d ‘Ivoire](https://www.ewarren.ca/) and [DigitalTransport4Africa (DT4A)](https://digitaltransport4africa.org) for the district of Abidjan in 2023. This smart mapping project is one of the four winners of the [DT4A innovation challenge](https://digitaltransport4africa.org/innovation-challenge/) organized by DT4A.

The data collection was conducted by the Ewarren team. The data collection was conducted across 490 routes in the 13 municipalities in the district of Abidjan and Grand Bassam from November 21st to May 31st, 2023.

**License**

The dataset is licensed under [Creative Commons Attribution-NonCommercial 4.0 International (CC BY-NC 4.0) License](https://creativecommons.org/licenses/by-nc/4.0/). For data usage, please clearly provide attribution and indicate if any modifications were made. Please contact us at [info@digitaltransport4africa.org](mailto:info@digitaltransport4africa.org) to request additional permissions.
